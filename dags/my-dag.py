import numpy as np
from airflow.decorators import dag, task
from airflow.utils.dates import days_ago
import logging


@dag("my_dag", start_date=days_ago(0), schedule="@daily")
def mat_mltiply():

    # Task 1
    @task(task_id="generate_mat")
    def generate_mat():

        logging.info("Generating numpy matrices...")
        A = np.random.randint(5, size=(4, 6))
        B = np.random.randint(5, size=(6, 8))

        logging.info("Saving generated numpy matrices...")
        np.savetxt('/opt/airflow/data/A.txt', A, delimiter=',')
        np.savetxt('/opt/airflow/data/B.txt', B, delimiter=',')
        
        return (A.tolist(), B.tolist())

    mats = generate_mat()

    # Task 2
    @task(task_id="multiply_mat")
    def multiply_mat():

        logging.info("Reading generated numpy matrices...")
        A = np.loadtxt("/opt/airflow/data/A.txt", delimiter=',')
        B = np.loadtxt("/opt/airflow/data/B.txt", delimiter=',')

        logging.info("Performing matrix multiplication...")
        res = np.matmul(A, B).tolist()

        logging.info("Saving result of matrix multiplication...")
        np.savetxt('/opt/airflow/data/AB.txt', res, delimiter=',')

        return (res)
    
    multiplyer = multiply_mat()

    mats >> multiplyer


mat_mltiply()