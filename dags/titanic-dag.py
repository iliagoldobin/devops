import numpy as np
from airflow.decorators import dag, task
from airflow.utils.dates import days_ago
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.preprocessing import MinMaxScaler
import pickle
import logging


@dag("titanic", start_date=days_ago(0), schedule="@daily")
def titanic():

    # Task 1
    @task(task_id="data-preprocessing")
    def preprocess_data():

        logging.info("Reading data...")
        df = pd.read_csv("/opt/airflow/data/titanic.csv")

        logging.info("Processing data...")
        df = df.drop(columns=["Cabin", "PassengerId", "Name", "Ticket"])
        df["Embarked"].fillna(df['Embarked'].value_counts().idxmax(), inplace=True)
        df["Age"].fillna(df["Age"].median(skipna=True), inplace=True)
        df = pd.get_dummies(df, columns=["Pclass","Embarked","Sex"], drop_first=True)
        
        logging.info("Saving processed data...")
        df.to_csv("/opt/airflow/data/titanic_preprocessed.csv")
        return (df)

    preprocessed_data = preprocess_data()

    # Task 2
    @task(task_id="random-forest-fitting")
    def fit_predict_RF_model():

        logging.info("Reading processed data...")
        df = pd.read_csv("/opt/airflow/data/titanic_preprocessed.csv")

        logging.info("Prefoorming train/test split...")
        X, y = df.drop(axis=1, columns=["Survived"]), df["Survived"]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=122, stratify=y)

        logging.info("Init model...")
        RF = RandomForestClassifier(n_estimators=200, max_depth=5, random_state=1)

        logging.info("Fitting model...")
        RF.fit(X_train, y_train)

        logging.info("Model predict test data...")
        y_pred = RF.predict(X_test)

        logging.info("Saving model...")
        with open("/opt/airflow/data/RF.pkl", "wb") as f:
            pickle.dump(RF, f)

        return (classification_report(y_test, y_pred))
    
    RF_model = fit_predict_RF_model()

    preprocessed_data >> RF_model

titanic()